from place import Place
from model import Model
from transition import Transition
from utils import display_start_message

p_1 = Place('[p_1]', 1)
p_2 = Place('[p_2]', 0)
p_3 = Place('[p_3]', 0)
p_4 = Place('[p_4]', 0)
p_5 = Place('[p_5]', 0)
p_6 = Place('[p_6]', 0)

t_1 = Transition('[t_1]', [p_1], [p_1, p_2])
t_2 = Transition('[t_2]', [p_2], [p_3])
t_3 = Transition('[t_3]', [p_2], [p_4])
t_4 = Transition('[t_4]', [p_2], [p_5])
t_5 = Transition('[t_5]', [p_3, p_4, p_5], [p_6])

positions = [p_1, p_2, p_3, p_4, p_5, p_6]
transactions = [t_1, t_2, t_3, t_4, t_5]

display_start_message(positions, 0)

model = Model(positions, transactions)
model.simulate(10, True)
