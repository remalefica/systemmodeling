import numpy as np


class Transition:
    def __init__(self, name, p_in, p_out, is_prior=False):
        self.name = name
        self.choice_probability = 0
        self.p_in = p_in
        self.p_out = p_out
        self.is_prior = is_prior
        self.is_active = False
        self.is_able_to_fire = False
        self.num_fire = 0
        self.edit_places()

    def edit_places(self):
        temp_p_in = self.p_in.copy()
        temp_p_out = self.p_out.copy()

        for i in range(len(self.p_in)):
            self.p_in[i] = np.array([temp_p_in[i], 1])
        for i in range(len(self.p_out)):
            self.p_out[i] = np.array([temp_p_out[i], 1])

    # check if transition is able to fire
    def check_possibility_to_fire(self, places):
        self.is_able_to_fire = True
        is_able_to_fire = True
        # for every arc in arcs_in of transition remember the previous place
        for a in self.p_in:
            input_place = a[0].name
            # find place in list of all places
            for i in range(len(places)):
                if input_place in places[i].name:
                    if a[0].num_tokens < a[1]:
                        is_able_to_fire = False
                        self.is_able_to_fire = False
                        break
        return is_able_to_fire

    def run_tokens(self, places, place, ver):
        str_in_out = ''
        new_places = self.define_input_or_output(place)
        for a in new_places:
            # number of tokens in/out
            for pl in places:
                if a[0].name == pl.name:
                    if place == 'output':
                        # вихід маркерів з переходу
                        pl.num_tokens += a[1]
                        break
                    elif place == 'input':
                        # вхід маркерів y перехід
                        pl.num_tokens -= a[1]
                        break
            if ver:
                str_in_out += f'{a[0].name}   '
        if ver:
            print(f'{place}:\t {str_in_out}')

    def add_arc_multiplicity(self, place, num, in_out):
        is_found = False
        places = self.define_input_or_output(in_out)
        for i in range(len(places)):
            if place.name == places[i][0].name:
                places[i][1] = num
                is_found = True
                break
        if not is_found:
            raise Exception(f'The place is not in the list of {in_out} places.')

    def define_input_or_output(self, in_out):
        if in_out == 'input':
            places = self.p_in
        elif in_out == 'output':
            places = self.p_out
        else:
            raise Exception('Method parameters must indicate whether place is input or output.')
        return places
