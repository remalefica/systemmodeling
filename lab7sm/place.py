class Place:
    def __init__(self, name, num_tokens):
        self.name = name
        self.num_tokens = num_tokens
        self.min_tokens = num_tokens if 0 < num_tokens else 0
        self.max_tokens = 0
        self.avg_tokens = 0
        self.conflict_transitions = []

    def token_total_statistics(self):
        self.token_avg_statistics()
        self.token_max_statistics()
        self.token_min_statistics()

    def token_avg_statistics(self):
        self.avg_tokens += self.num_tokens

    def token_max_statistics(self):
        if self.num_tokens > self.max_tokens:
            self.max_tokens = self.num_tokens

    def token_min_statistics(self):
        if self.num_tokens < self.min_tokens:
            self.min_tokens = self.num_tokens
