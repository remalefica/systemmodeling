from place import Place
from model import Model
from transition import Transition
from utils import display_start_message

n = 2

p_input_1 = Place('[p_input_1]', 1)
p_queue_1 = Place('[p_queue_1]', 0)
p_put_process = Place('[p_put_process]', 0)
p_buffer = Place('[p_buffer]', 0)
p_buffer_limit = Place('[p_buffer_limit]', n)
p_input_2 = Place('[p_input_2]', 1)
p_queue_2 = Place('[p_queue_2]', 0)
p_take_process = Place('[p_take_process]', 0)
p_done = Place('[p_done]', 0)

t_come_1 = Transition('[t_come_1]', [p_input_1], [p_input_1, p_queue_1])
t_PRODUCER_in = Transition('[t_PRODUCER_in]', [p_queue_1, p_buffer_limit], [p_put_process])
t_PRODUCER_out = Transition('[t_PRODUCER_out]', [p_put_process], [p_buffer])
t_come_2 = Transition('[t_come_2]', [p_input_2], [p_input_2, p_queue_2])
t_CONSUMER_in = Transition('[t_CONSUMER_in]', [p_queue_2, p_buffer], [p_take_process])
t_CONSUMER_out = Transition('[t_CONSUMER_out]', [p_take_process], [p_buffer_limit, p_done])

positions = [p_input_1, p_queue_1, p_put_process, p_buffer, p_buffer_limit,
             p_input_2, p_queue_2, p_take_process, p_done]
transactions = [t_come_1, t_PRODUCER_in, t_PRODUCER_out,
                t_come_2, t_CONSUMER_in, t_CONSUMER_out]

display_start_message(positions, 0)

model = Model(positions, transactions)
model.simulate(100, True)
