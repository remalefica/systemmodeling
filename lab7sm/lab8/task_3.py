from place import Place
from lab8.model_3 import Model3
from transition import Transition
from utils import display_start_message

p_input = Place('[p_input]', 1)
p_input_done = Place('[p_input_done]', 0)
p_queue_1 = Place('[p_queue_1]', 0)
p_queue_2 = Place('[p_queue_2]', 0)
p_queue_3 = Place('[p_queue_3]', 0)
p_in_progress_1 = Place('[p_in_progress_1]', 0)
p_in_progress_2 = Place('[p_in_progress_2]', 0)
p_in_progress_3 = Place('[p_in_progress_3]', 0)
p_done_1 = Place('[p_done_1]', 0)
p_done_2 = Place('[p_done_2]', 0)
p_done_3 = Place('[p_done_3]', 0)
p_resources = Place('[p_resources]', 6)


t_start = Transition('[t_start]', [p_input], [p_input, p_input_done])
t_in_1 = Transition('[t_in_1]', [p_input_done], [p_queue_1])
t_start_1 = Transition('[t_start_1]', [p_queue_1, p_resources], [p_in_progress_1])
t_finish_1 = Transition('[t_finish_1]', [p_in_progress_1], [p_resources, p_done_1])
t_in_2 = Transition('[t_in_2]', [p_input_done], [p_queue_2])
t_start_2 = Transition('[t_start_2]', [p_queue_2, p_resources], [p_in_progress_2])
t_finish_2 = Transition('[t_finish_2]', [p_in_progress_2], [p_resources, p_done_2])
t_in_3 = Transition('[t_in_3]', [p_input_done], [p_queue_3])
t_start_3 = Transition('[t_start_3]', [p_queue_3, p_resources], [p_in_progress_3])
t_finish_3 = Transition('[t_finish_3]', [p_in_progress_3], [p_resources, p_done_3])

t_start_1.add_arc_multiplicity(p_resources, 6, 'input')
t_start_2.add_arc_multiplicity(p_resources, 2, 'input')
t_start_3.add_arc_multiplicity(p_resources, 3, 'input')
t_finish_1.add_arc_multiplicity(p_resources, 6, 'output')
t_finish_2.add_arc_multiplicity(p_resources, 2, 'output')
t_finish_3.add_arc_multiplicity(p_resources, 3, 'output')

positions = [p_input, p_input_done, p_resources,
             p_queue_1, p_queue_2, p_queue_3,
             p_in_progress_1, p_in_progress_2, p_in_progress_3,
             p_done_1, p_done_2, p_done_3]
transactions = [t_start, t_in_1, t_start_1, t_finish_1,
                t_in_2, t_start_2, t_finish_2,
                t_in_3, t_start_3, t_finish_3]

display_start_message(positions, 0)

model = Model3(positions, transactions)
model.simulate(30, True)
