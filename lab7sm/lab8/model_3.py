from model import Model
from utils import get_ratio


class Model3(Model):
    def __init__(self, places, transitions):
        super().__init__(places, transitions)

    def statistics_places(self):
        df = super().statistics_places()
        ratio = get_ratio([df['max'].values[9], df['max'].values[10], df['max'].values[11]])[0]
        s = 'The ratio of the completed tasks\' number: '
        for r in ratio:
            s += f'{r}:'
        print(f'{s[:-1]}\n')
        return df


