from place import Place
from model import Model
from transition import Transition
from utils import display_start_message

p_A_to_B = Place('[p_A_to_B]', 1)
p_B_to_A = Place('[p_B_to_A]', 1)
p_11 = Place('[p_11]', 0)
p_12 = Place('[p_12]', 0)
p_13 = Place('[p_13]', 0)
p_14 = Place('[p_14]', 1)
p_21 = Place('[p_21]', 0)
p_22 = Place('[p_22]', 0)
p_23 = Place('[p_23]', 0)
p_24 = Place('[p_24]', 1)
p_control_signal = Place('[p_control_signal]', 1)

t_transfer_request_A_to_B = Transition('[t_transfer_request_A_to_B]', [p_A_to_B, p_14], [p_11])
t_transfer_request_B_to_A = Transition('[t_transfer_request_B_to_A]', [p_B_to_A, p_24], [p_21])
t_send_message_A = Transition('[t_send_message_A]', [p_11, p_control_signal], [p_12])
t_send_message_B = Transition('[t_send_message_B]', [p_21, p_control_signal], [p_22])
t_get_in_B = Transition('[t_get_in_B]', [p_12], [p_13])
t_get_in_A = Transition('[t_get_in_A]', [p_22], [p_23])
t_success_in_B = Transition('[t_success_in_B]', [p_13], [p_14, p_A_to_B, p_control_signal])
t_success_in_A = Transition('[t_success_in_A]', [p_23], [p_24, p_B_to_A, p_control_signal])


positions = [p_A_to_B, p_B_to_A, p_11, p_12, p_13, p_14,
             p_21, p_22, p_23, p_24, p_control_signal]
transactions = [t_transfer_request_A_to_B, t_transfer_request_B_to_A,
                t_send_message_A, t_send_message_B,
                t_get_in_B, t_get_in_A,
                t_success_in_B, t_success_in_A]

display_start_message(positions, 0)

model = Model(positions, transactions)
model.simulate(30, True)
