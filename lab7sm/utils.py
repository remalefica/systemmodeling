from termcolor import colored


def display_start_message(pl, i):
    notification = f'\n#{i + 1}. START'
    print(colored(notification, 'blue'))
    display_num_tokens_in_places(pl)


def get_rid_of_duplicates(array):
    new_conflict_transitions = []
    for elem in array:
        if elem not in new_conflict_transitions:
            new_conflict_transitions.append(elem)
    return new_conflict_transitions

def display_num_tokens_in_places(places):
    num_tokens_in_places = ''
    for p in places:
        num_tokens_in_places += f'{p.name}={p.num_tokens}  '
    print(f'TOKENS\n{num_tokens_in_places}\n')


def display_changes(i):
    notification = ''
    if i == 0:
        notification = f'Basic.'
    if i == 1:
        notification = f'Set priority to parts manufacturing (type 1).'
    if i == 2:
        notification = f'Increase number of parts manufacturing (type 1) to 40.'
    if i == 3:
        notification = f'Reduce number of parts manufacturing (type 2) to 5.'
    if i == 4:
        notification = f'Reduce number of parts (type 1) that are needed to create product to 5.'
    if i == 5:
        notification = f'There are 400 ready parts in warehouse (type 1).'
    if i == 6:
        notification = f'There are 400 ready parts in warehouse (both types).'

    print(colored('\nVERIFICATION\n', 'magenta')+notification)


def get_ratio(array):
    compression_is_possible, i = check_ratio(array)
    if compression_is_possible:
        for n in range(len(array)):
            array[n] //= i
        array, stop = get_ratio(array)
    else:
        stop = True
    return array, stop


def check_ratio(array):
    compression_is_possible, i = True, 0
    if len(set(array)) <= 1:
        return compression_is_possible, array[0]

    for i in range(9, 1, -1):
        compression_is_possible = True
        for n in array:
            if n % i != 0:
                compression_is_possible = False
                break
    return compression_is_possible, i
