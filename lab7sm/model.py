import random
import pandas as pd
import numpy as np
from utils import display_num_tokens_in_places, get_rid_of_duplicates


class Model:
    def __init__(self, places, transitions):
        self.places = places
        self.transitions = transitions
        self.next_transitions = list()
        self.verification = False
        self.t_curr = 0
        self.num_iterations = 0
        self.conflict_transitions = []  # [[p, [transitions]],..]
        self.define_conflict_transitions()

    def simulate(self, num_iterations, ver):
        self.num_iterations = num_iterations
        self.verification = ver

        for p in self.places:
            p.token_total_statistics()

        self.define_next_transitions()
        if not self.stop_imitation_condition():
            self.run_transitions('input')
            while self.t_curr < self.num_iterations:
                if self.verification:
                    print(f'_________\n\nTACT {self.t_curr + 1}')

                self.run_transitions('output')
                self.next_transitions.clear()
                for p in self.places:
                    p.token_avg_statistics()

                if self.verification:
                    print(' ')
                    display_num_tokens_in_places(self.places)

                self.define_next_transitions()
                if self.stop_imitation_condition():
                    break
                self.run_transitions('input')

                self.t_curr += 1
        self.print_statistics()

    def stop_imitation_condition(self):
        is_stop = False
        if not self.next_transitions:
            self.num_iterations = self.t_curr + 1
            if self.verification:
                print('The imitation is stopped.')
                is_stop = True
        return is_stop

    def define_next_transitions(self):
        next_transitions = list()
        for t in self.transitions:
            if t.check_possibility_to_fire(self.places):
                next_transitions.append(t)
        self.next_transitions = next_transitions

    def define_conflict_transitions(self):
        pairs = []
        for i in range(len(self.transitions)):
            for p_in in self.transitions[i].p_in:
                pairs.append([i, self.transitions[i], p_in[0]])    # [index, transition, p_in]
        pairs = np.array(pairs, dtype=object)

        conflict_transitions = []
        for pair in pairs:
            conflict = []
            for other_pair in pairs:
                if pair[0] != other_pair[0] and pair[2] == other_pair[2]:
                    if len(conflict) == 0:
                        conflict.append(pair[1])
                    conflict.append(other_pair[1])
            if len(conflict) != 0:
                conflict_transitions.append([pair[2], sorted(conflict, key=lambda x: x.name)])

        self.conflict_transitions = np.array(get_rid_of_duplicates(conflict_transitions), dtype=object)

    def run_transitions(self, in_out):
        if in_out == 'output':
            for t in self.transitions:
                if t.is_active:
                    self.run_transition(t, in_out)

        if in_out == 'input':
            # define conflict transitions
            list_of_conflict_transitions = []
            for tc in self.conflict_transitions:
                for transition in tc[1]:
                    list_of_conflict_transitions.append(transition)
            # run all next_transitions with no conflicts
            i = 0
            for t in self.next_transitions:
                if t not in list_of_conflict_transitions:
                    self.run_transition(self.next_transitions[i], in_out)
                i += 1
            # solve conflicts
            for con_t in self.conflict_transitions:
                self.solve_conflict(con_t[1], in_out)

    def solve_conflict(self, con_t, in_out):
        next_conflict_transitions = self.get_probability_conflicts(con_t)

        # random choice
        r = random.random()
        is_able_to_fire = False
        for i in range(len(next_conflict_transitions)):
            if r < next_conflict_transitions[i].choice_probability:
                self.run_transition(next_conflict_transitions[i], in_out)
                next_conflict_transitions.pop(i)
                # check_possibility_to_fire of any transitions left
                if len(next_conflict_transitions) != 0:
                    for nct in next_conflict_transitions:
                        nct.check_possibility_to_fire(self.places)
                # check every transition if it is able to fire
                new_next_conflict_transitions = []
                for nct in next_conflict_transitions:
                    if nct.is_able_to_fire:
                        new_next_conflict_transitions.append(nct)

                next_conflict_transitions = self.solve_conflict(new_next_conflict_transitions, in_out)
                break
            else:
                r -= next_conflict_transitions[i].choice_probability
        return next_conflict_transitions

    def run_transition(self, transition, in_out):
        if in_out == 'input':
            if self.verification:
                print(f'\nTRANSITION {transition.name}\t')
            # вхід маркерів y перехід
            transition.run_tokens(self.places, 'input', self.verification)
            transition.is_active = True
            for k in range(len(transition.p_in)):
                transition.p_in[k][0].token_min_statistics()
        if in_out == 'output':
            # вихід маркерів з переходу
            if transition.is_active:
                if self.verification:
                    print(f'\nTRANSITION {transition.name}\t')
                transition.run_tokens(self.places, 'output', self.verification)
                transition.is_active = False
                for j in range(len(transition.p_out)):
                    transition.p_out[j][0].token_max_statistics()
                # update statistics about transitions
                transition.num_fire += 1

    def get_probability_conflicts(self, transitions):
        next_conflict_transitions = []

        for t in transitions:
            if t in self.next_transitions:
                next_conflict_transitions.append(t)

        # find out if there are next transitions with priority
        prior_transitions = list()
        for tn in next_conflict_transitions:
            if tn.is_prior:
                prior_transitions.append(tn)
        # if no priority
        if len(prior_transitions) == 0:
            for tn in next_conflict_transitions:
                tn.choice_probability = float(1) / len(next_conflict_transitions)
        # if there are prior transitions
        else:
            for tn in next_conflict_transitions:
                tn.choice_probability = 0
            for tnp in prior_transitions:
                tnp.choice_probability = float(1) / len(prior_transitions)

        return next_conflict_transitions

    def print_statistics(self):
        print(f'\nSTATISTICS')
        pd.options.display.max_columns = None
        pd.options.display.width = None
        df = self.statistics_places()
        print(df.to_string(index=False))
        df = self.statistics_transitions()
        print('\n' + df.to_string(index=False))

    def statistics_places(self):
        names, min_m, max_m, avg_m = list(), list(), list(), list()
        for p in self.places:
            p.avg_tokens /= self.num_iterations + 1
            names.append(p.name)
            min_m.append(p.min_tokens)
            max_m.append(p.max_tokens)
            avg_m.append(p.avg_tokens)
        return pd.DataFrame({'name': names, 'min': min_m, 'max': max_m, 'avg': avg_m})

    def statistics_transitions(self):
        names, num_fire = list(), list()
        for t in self.transitions:
            names.append(t.name)
            num_fire.append(t.num_fire)
        return pd.DataFrame({'name': names, 'fires': num_fire})
