from place import Place
from model import Model
from transition import Transition
from utils import display_start_message, display_changes

for i in range(7):
    p_income_1 = Place('[p_income_1]', 1)
    p_income_2 = Place('[p_income_2]', 1)
    p_warehouse_1 = Place('[p_warehouse_1]', 0)
    p_warehouse_2 = Place('[p_warehouse_2]', 0)
    p_ready = Place('[p_ready]', 0)
    p_sec_income = Place('[p_sec_income]', 1)
    p_sec_ready = Place('[p_sec_ready]', 0)
    p_sec_empty = Place('[p_sec_empty]', 0)

    t_production_1 = Transition('[t_production_1]', [p_income_1], [p_income_1, p_warehouse_1])
    t_production_2 = Transition('[t_production_2]', [p_income_2], [p_income_2, p_warehouse_2])
    t_production_ready = Transition('[t_production_ready]', [p_warehouse_1, p_warehouse_2, p_sec_ready],
                                    [p_ready], is_prior=True)
    t_sec_come = Transition('[t_sec_come]', [p_sec_income], [p_sec_income, p_sec_ready])
    t_sec_empty = Transition('[t_sec_empty]', [p_sec_ready], [p_sec_empty])

    t_production_1.add_arc_multiplicity(p_warehouse_1, 10, 'output')
    t_production_2.add_arc_multiplicity(p_warehouse_2, 40, 'output')
    t_production_ready.add_arc_multiplicity(p_warehouse_1, 20, 'input')
    t_production_ready.add_arc_multiplicity(p_warehouse_2, 20, 'input')

    if i == 1:
        t_production_1.is_prior = True
    if i == 2:
        t_production_1.add_arc_multiplicity(p_warehouse_1, 40, 'output')
    if i == 3:
        t_production_2.add_arc_multiplicity(p_warehouse_2, 5, 'output')
    if i == 4:
        t_production_ready.add_arc_multiplicity(p_warehouse_1, 5, 'input')
    if i == 5 or i == 6:
        p_warehouse_1.num_tokens = 400
    if i == 6:
        p_warehouse_2.num_tokens = 400

    places = [p_income_1, p_income_2, p_warehouse_1, p_warehouse_2, p_ready,
              p_sec_income, p_sec_ready, p_sec_empty]
    transitions = [t_production_1, t_production_2, t_production_ready,
                   t_sec_come, t_sec_empty]

    display_start_message(places, i)

    model = Model(places, transitions)
    model.simulate(10, True) if i == 0 else model.simulate(10, False)
    display_changes(i)
