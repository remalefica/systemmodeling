from universal_algorithm.elements.create import Create


class BankCreate(Create):
    def __init__(self, delay, delay_dev=None, name_of_element=None):
        super().__init__(delay, delay_dev, name_of_element)

    def out_act(self):
        # виконуємо збільшення лічильника кількості
        self.quantity += 1
        # встановлюємо коли пристрій буде вільним
        self.t_next[0] = self.t_curr + super().get_delay()  # встановлюємо час звільнення пристрою

        self.choose_next_element()
