from universal_algorithm.model import Model
import hospital.all_patients as al


class HospitalModel(Model):
    def __init__(self, elements: list):
        super().__init__(elements)
        self.time_in_bank = 0

    def result_global(self):
        self.calculate_global_values()
        self.print_global_values('\nHOSPITAL STATISTICS')

    def print_global_values(self, header):
        super().print_global_values(header)
        print(f'avg time to lab = {al.calculate_avg_to_lab()}')
        print('\nFor P1: to ward; For P2 and P3: from lab')
        for i in range(3):
            print(f'PATIENT TYPE #{i+1}: avg time in hospital= {al.calculate_avg_time_p(i+1)}')




