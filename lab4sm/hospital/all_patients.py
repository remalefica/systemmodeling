from hospital.patient import Patient


def init():
    global patients
    patients = []


def get_patients():
    return patients


def get_index(index):
    return patients[index].index


def get_in_queue_num(index):
    return patients[index].in_queue_num


def get_in_queue_name(index):
    return patients[index].in_queue_name


def get_delay(index):
    return patients[index].p_type.delay


def get_delay_dev(index):
    return patients[index].p_type.delay_dev


def set_t_lab_in(index, t_lab_in):
    patients[index].t_lab_in = t_lab_in


def set_status_fail(index, fail):
    patients[index].status_fail = fail


def get_p_type_index(index):
    return patients[index].p_type.index


def get_t_next(index):
    return patients[index].t_next


def add(index, t_curr, p_type):
    patient = Patient(index, t_curr, p_type)
    patients.append(patient)


def update_t_next(index, t_next):
    for p in patients:
        if p.index == index:
            p.t_next = t_next
            break


def save_to_queue(index, name, q_num):
    patients[index].in_queue_name = name
    patients[index].in_queue_num = q_num


def from_queue(index):
    patients[index].in_queue_num = 0
    patients[index].in_queue_name = None


def calculate_avg_time_p(num):
    overall_time = 0
    number_of_p = 0
    for p in get_patients():
        if p.t_next is not None and p.status_fail is False:
            if num == 1 and p.dispose is not None:
                number_of_p += 1
                overall_time += p.dispose - p.creation_time
            elif p.old_p_type == num and p.t_lab_out is not None:
                number_of_p += 1
                overall_time += p.t_lab_out - p.creation_time

    if number_of_p == 0:
        number_of_p = 1

    return overall_time/number_of_p


def calculate_avg_to_lab():
    overall_time = 0
    number_of_p = 0
    for p in get_patients():
        if p.t_lab_in is not None:
            number_of_p += 1
            overall_time += p.t_lab_in - p.creation_time
    return overall_time / number_of_p


def change_type(index, from_type, to_type):
    if patients[index].p_type.index == from_type:
        patients[index].old_p_type = patients[index].p_type.index
        patients[index].p_type.index = int(to_type)
        patients[index].t_lab_out = patients[index].t_next
