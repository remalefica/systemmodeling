from termcolor import colored
import numpy as np
from universal_algorithm.elements.process import Process
import hospital.all_patients as al


class HospitalProcess(Process):
    def __init__(self, delay, delay_dev=None, channel=1, name_of_element=None):
        super().__init__(delay, delay_dev, channel, name_of_element)
        self.queue = [0] * 3
        self.by_type = None
        self.type_priority = None
        self.delay_by_data = False
        if isinstance(delay, list):
            self.delay_by_data = True
        self.change_patient_type = None

    def set_change_patient_type(self, prop):
        self.change_patient_type = np.array(prop)

    def in_act(self, index=None):
        free_devices = self.get_free_devices()
        if len(free_devices) > 0:
            for i in free_devices:
                self.states[i] = 1
                # set час звільнення пристрою
                self.delay_time(i, index)

                al.update_t_next(index, self.t_next[i])
                notification = f'Patient will be processed at {al.get_t_next(index)}'
                print(colored(notification, 'green'))
                break

        else:
            if sum(self.queue) < self.max_queue:
                self.queue[al.get_p_type_index(index) - 1] += 1
                # максимальне спостережуване значення черги
                if self.max_observed_queue < sum(self.queue):
                    self.max_observed_queue = sum(self.queue)

                al.save_to_queue(index, self.name, sum(self.queue))

                notification = f'Patient {index + 1} is {al.get_in_queue_num(index)} ' \
                               f'in queue {al.get_in_queue_name(index)}'
                print(colored(notification, 'yellow'))
            else:
                self.failure += 1
                al.set_status_fail(index, True)

    def out_act(self):
        # визначити, які канали є поточними
        current_channels = self.get_current_devices()
        for i in current_channels:
            # збільшення кількості лічильника
            self.quantity += 1
            # index and type of next_element
            index, p_type, break_queue = -1, 0, 1

            # пристрій завершив роботу
            self.t_next[i] = float('inf')
            self.states[i] = 0
            if sum(self.queue) > 0:
                next_in_queue = float('inf')
                for p in al.get_patients():
                    if not p.status_fail and self.name == p.in_queue_name:
                        # вибір елементу з черги
                        if self.type_priority == p.p_type.index:
                            index = p.index
                            p_type = p.p_type.index - 1
                            break_queue = p.in_queue_num
                            break
                        elif p.in_queue_num < next_in_queue:
                            index = p.index
                            p_type = p.p_type.index - 1
                self.queue[p_type] -= 1

                # змінити id черги після break_queue
                for p in al.get_patients():
                    if not p.status_fail:
                        if self.name == p.in_queue_name and p.in_queue_num > break_queue:
                            p.in_queue_num -= 1

                al.from_queue(index)
                self.delay_time(i, index)
                self.states[i] = 1
            else:
                for p in al.get_patients():
                    if not p.status_fail:
                        if p.t_next is not None:
                            if p.t_next == self.t_curr:
                                index = p.index
                                break

            if self.change_patient_type is not None:
                for j in range(len(self.change_patient_type)):
                    al.change_type(index, self.change_patient_type[j][0], self.change_patient_type[j][1])

            if self.next_element is not None:
                self.choose_next_element(index=index)

            # lab in
            if self.name == 'FROM_RECEPTION_TO_LAB':
                al.set_t_lab_in(index, self.t_curr)

    def delay_time(self, i, index):
        if self.delay_by_data is True:
            self.t_next[i] = self.t_curr + super().get_delay(
                delay_mean=al.get_delay(index),
                delay_dev=al.get_delay_dev(index))
        else:
            self.t_next[i] = self.t_curr + super().get_delay()

    def choose_next_element(self, index=None):
        # пріоритетність чи ймовірність
        if self.by_type is not None:
            p_type_index = al.get_p_type_index(index)
            next_element = self.by_type[p_type_index]
            next_element.in_act(index)
        elif self.probability != [1] and self.priority != [1]:
            raise Exception('Route selection is ambiguous: probability and priority are set simultaneously')
        elif self.probability != [1]:
            next_element = np.random.choice(a=self.next_element, p=self.probability)
            next_element.in_act(index)
        elif self.priority != [1]:
            next_element = self.choose_by_priority()
            next_element.in_act(index)
        elif self.probability == [1] and self.priority == [1]:
            self.next_element[0].in_act(index)

    def calculate(self, delta):
        # для обчислення середнього значення довжини черги
        for q in self.queue:
            self.mean_queue += q * delta

        for i in range(self.channel):
            self.mean_load += self.states[i] * delta
