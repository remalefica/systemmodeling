from hospital.hospital_model import HospitalModel
from hospital.hospital_create import HospitalCreate
from hospital.hospital_process import HospitalProcess
from hospital.hospital_dispose import HospitalDispose

from hospital.patient_type import PatientType
import hospital.all_patients as all


def task_hospital():
    all.init()

    c = HospitalCreate(5)

    pt_1 = PatientType(1, 15, 5)
    pt_2 = PatientType(2, 40, 5)
    pt_3 = PatientType(3, 30, 5)

    c.data_types = [pt_1, pt_2, pt_3]
    c.type_probability = [0.5, 0.1, 0.4]

    p_rd = HospitalProcess([pt_1.delay, pt_2.delay, pt_3.delay], channel=4, name_of_element='RECEPTION')
    p_rd.delay_by_data = True
    p_to_ward = HospitalProcess(3, 8, channel=3, name_of_element='FROM_RECEPTION_TO_WARD')
    p_to_lab = HospitalProcess(2, 5, channel=40, name_of_element='FROM_RECEPTION_TO_LAB')
    p_from_lab = HospitalProcess(2, 5, channel=40, name_of_element='FROM_LAB_TO_RECEPTION')
    p_registry = HospitalProcess(4.5, 3, channel=1, name_of_element='REGISTRY_IN_LAB')
    p_lab = HospitalProcess(4, 2, channel=2, name_of_element='LAB')

    d_ward = HospitalDispose('WARD')
    d_leave = HospitalDispose('LEAVE')

    c.distribution = 'exp'
    p_rd.distribution = 'exp'
    p_to_ward.distribution = 'unif'
    p_to_lab.distribution = 'unif'
    p_from_lab.distribution = 'unif'
    p_registry.distribution = 'erlang'
    p_lab.distribution = 'erlang'

    p_rd.max_queue = float('inf')
    p_to_ward.max_queue = 0
    p_to_lab.max_queue = 0
    p_from_lab.max_queue = 0
    p_registry.max_queue = float('inf')
    p_lab.max_queue = float('inf')

    c.next_element = [p_rd]

    p_rd.type_priority = 1
    p_rd.next_element = [p_to_ward, p_to_lab]
    p_rd.by_type = {1: p_to_ward, 2: p_to_lab, 3: p_to_lab}

    p_to_ward.next_element = [d_ward]
    p_to_lab.next_element = [p_registry]
    p_registry.next_element = [p_lab]

    p_lab.next_element = [d_leave, p_from_lab]
    p_lab.probability = [0.5, 0.5]

    p_from_lab.next_element = [p_rd]
    p_from_lab.set_change_patient_type([[2, 1], [3, 1]])

    elements = [c, p_rd, p_to_ward, p_to_lab, p_registry, p_lab, p_from_lab, d_ward, d_leave]
    model = HospitalModel(elements)
    model.simulate(1000.0, flag=True)
