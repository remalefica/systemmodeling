class Patient:
    def __init__(self, index, creation_time, p_type):
        self.index = index
        self.creation_time = creation_time
        self.t_next = None
        self.status_fail = False

        self.in_queue_name = None
        self.in_queue_num = 0

        self.old_p_type = None
        self.p_type = p_type

        self.t_lab_in = None
        self.dispose = None
        self.t_lab_out = None


