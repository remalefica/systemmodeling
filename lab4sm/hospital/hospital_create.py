import numpy as np
from termcolor import colored

from universal_algorithm.elements.create import Create
import hospital.all_patients as al


class HospitalCreate(Create):
    def __init__(self, delay, delay_dev=None, name_of_element=None):
        super().__init__(delay, delay_dev, name_of_element)
        self.type_probability = [1]

    def out_act(self):
        # виконуємо збільшення лічильника кількості
        self.quantity += 1
        # пацієнт прийшов до лікарні
        al.add(self.quantity-1, self.t_curr, self.rand_patient_type())
        # встановлюємо коли пристрій буде вільним
        self.t_next[0] = self.t_curr + super().get_delay()  # встановлюємо час звільнення пристрою

        notification = f'Patient {al.get_index(self.quantity-1) + 1} ' \
                       f'(Type {al.get_p_type_index(self.quantity-1)}) ' \
                       f'arrived at {al.patients[self.quantity-1].creation_time}'
        print(colored(notification, 'blue'))

        # t_next of patient
        self.choose_next_element(al.get_index(self.quantity-1))
        # notification = f'Patient will be registered at {al.patients[index].t_next}'
        # print(colored(notification, 'red'))

    def rand_patient_type(self):
        if self.type_probability != [1]:
            patient = np.random.choice(a=self.data_types, p=self.type_probability)
            return patient

    def choose_next_element(self, index=None):
        # пріоритетність чи ймовірність
        if self.probability != [1] and self.priority != [1]:
            raise Exception('Route selection is ambiguous: probability and priority are set simultaneously')
        elif self.probability != [1]:
            next_element = np.random.choice(a=self.next_element, p=self.probability)
            next_element.in_act(index)
        elif self.priority != [1]:
            next_element = self.choose_by_priority()
            next_element.in_act(index)
        elif self.probability == [1] and self.priority == [1]:
            self.next_element[0].in_act(index)
