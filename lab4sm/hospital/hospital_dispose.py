from universal_algorithm.elements.dispose import Dispose
import hospital.all_patients as al


class HospitalDispose(Dispose):
    def __init__(self, name_of_element=None):
        super().__init__(name_of_element)

    def in_act(self, index=None, delay_by_data=None):
        if self.states[0] == 0:
            self.states[0] = 1
            self.t_next[0] = self.t_curr + super().get_delay()  # set час звільнення пристрою
            al.patients[index].dispose = self.t_next[0]
